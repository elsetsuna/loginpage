<?php 
date_default_timezone_set('Asia/Jakarta');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html xmlns:fb="https://www.facebook.com/2008/fbml">
<!--<![endif]-->
<?php
require_once("admin/config/db.php");
//include '../config/db.php';
include 'admin/config/setting.php';
include 'admin/function/function.php';
?>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <base id="pageBase" ></base>
    <meta name="viewport" content="width=device-width">
    <title>Hawaii Lottery</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    

    
        <link rel="icon" href="favicon.ico" />
        <link rel="shortcut icon" href="favicon.ico" />

        <link rel="stylesheet" href="css/open-sans9ce4.css?=30" />
        <link rel="stylesheet" href="css/main9ce4.css?=30" />
       
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
        <script src="js/1.11.0/jquery.mina097.js?=38"></script>
        
<style>
    .common_head {
  background:url(img/common_head.gif) 0 0 repeat-x;
  height:39px;
  line-height:39px;
  padding-left:10px;
  color:#2e2c2c;
  border-left:1px solid #c5c5c6;
  border-right:1px solid #c5c5c6;
  font-size:18px;
}
.side_chart {
  background:#dddda7;
  padding:15px 9px 15px 9px;
}
.chart_2 {
}
.chart_2 tr td {
  background:#F1F0F4;
  border-bottom:1px solid #dddda7;
  color:#323233;
  font-weight:bold;
  padding:2px 5px 2px 5px;
}
.chart_2 tr.padd td {
  padding:9px 5px 9px 5px;
}
.chart_2 tr td a {
  background:url(img/arrow.gif) 0 4px no-repeat;
  padding:0 0 0 15px;
  color:#323233;
}
.chart_2 tr td a:hover {
  color:#947d09;
}
    .live 
{ 
    
  font-size:30px; 
  position:center;
  color:#fff !important;  
  animation:live 0.75s ease-out infinite; 

    
}
@keyframes live{  
  from{   
    text-shadow:
    0px 0px 3px      #007FFF,   
    0px 0px 3px      #007FFF,     
    0px 0px 15px    #007FFF,    
    0px 0px 15px    #007FFF,    
    0px 0px 15px    #007FFF,    
    0px 0px 15px    #007FFF,    
    0px 0px 15px    #007FFF,    
    0px 0px 15px    #007FFF,    
    0px 0px 30px    #007FFF,    
    0px 0px 30px    #007FFF,    
    0px 0px 30px      #C9FF26,    
    0px 0px 50px      #C9FF26,    
    0px 10px 60px     #C9FF26,    
    0px 10px 60px     #C9FF26,    
    0px 10px 60px     #C9FF26,    
    0px 10px 60px     #C9FF26,    
    0px -10px 60px    #C9FF26,    
  0px -10px 60px        #C9FF26;
  } 
}

  </style>
      <script language="JAVASCRIPT" type="TEXT/JAVASCRIPT">
      <!--
      var livedrawWindow=null;
      function livedraw(mypage,myname,w,h,pos,infocus){
        
        if (pos == 'random')
        {LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
        else
        {LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
        settings='width='+ w + ',height='+ h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';livedrawWindow=window.open('',myname,settings);
        if(infocus=='front'){livedrawWindow.focus();livedrawWindow.location=mypage;}
        if(infocus=='back'){livedrawWindow.blur();livedrawWindow.location=mypage;livedrawWindow.blur();}
        
      }
      // -->
    </script>
    <!--[if IE 6]>
		<script type="text/javascript" src="js/config.js"></script>
		<script type="text/javascript" src="js/ie6png.min.js"></script>
		<script type="text/javascript" language="javascript">
						DD_belatedPNG.fix('img');
		</script>
		<![endif]-->
    <!--[if IE 7]>
			<link href="css/IE7.css" rel="stylesheet" type="text/css" />
		<![endif]-->
    <!--[if gte IE 9]>
		  <style type="text/css">
			.gradient {
			   filter: none;
			}
		  </style>
		<![endif]-->
</head>
<body>



        <div id="layout-wrapper" class="main-page">
        <div class="header">
            <header id="layout-header">
                <div id="header" class="result">
                    <div class="logo">
                        <a href="index.php" id="site-logo" title="Hawaii Lottery"></a>
                        <div id="logo"></div>
                    </div>
                    <div id="pnlTopLogin" class="login">
                        <div id="Panel2" style="padding-top: 2px; margin-left: 39px;">
                                <span style="margin-right: 6px;">Official Hawaii Lottery</b></span>
                        </div>
                    </div>

                </div>
            </header>

            </div>
            <!-- BEGIN: PAGE HEADER -->
            	
  <div class="bonus">
    <div class="bonus-wrapper">
        <h2 class="hero-3">Welcome to</h2> <h2 class="hero-4"><span style="color:rgb(237, 153, 16);"> Hawaii</span> Lottery</h2>
    </div>
</div>
<br/><br/><br/><br/>
<div id="content" align="center" style=";padding-bottom: 0px;background-color: rgba(255, 255, 255, 0);">
<a class="live" style="
    outline: none;
    text-decoration: none;" href="javascript:livedraw(&#39;livedraw/&#39;,&#39;LIVE DRAW&#39;,&#39;800&#39;,&#39;500&#39;,&#39;center&#39;,&#39;front&#39;);">   LIVE DRAW AT : 05:55 GMT -10  </a>
  </div>
<div id="content" style="">
                <!-- BEGIN: PAGE CONTENT -->
                
          <div class="content-border"></div>
          <div id="bottom-shadow"></div>
                <!-- END: PAGE CONTENT -->
                <div class="content-text">
                                    <div class="row">
                    <div class="col-xs-9">
                    <div class="table-responsive white_section">
                  <table class="table timetable_big" style="padding: 0px;">
                    <thead class="theader">
                      <tr>
                        <th style="width:9%;"><strong>Date</strong></th>
                        <th style="width:2%;"><strong>Day</strong></th>
                        <th style="width:15%;"><strong>Prize 1</strong></th>
                        <th style="width:6%;"><strong>Prize 2</strong></th>
                        <th style="width:6%;"><strong>Prize 3</strong></th>
                        <th style="width:17%;"><strong>Starter Prize</strong></th>
                        <th style="width:17%;"><strong>Consolation Prize</strong></th>
                      </tr>
                    </thead>
                    <tbody>
<?php

if(isset($_GET['day'])){
  $day = $_GET['day'];
  if($day == 'sunday'){
    $qgetsum = "SELECT * FROM periode WHERE status = '1' AND hari_keluar='$day' LIMIT 200";
       getday('sunday','1');
    if(isset($_GET['count'])){
      $cnt = $_GET['count'];
      getday('sunday',$cnt);
    }
  }elseif($day == 'monday'){
    $qgetsum = "SELECT * FROM periode WHERE status = '1' AND hari_keluar='$day' LIMIT 200";
       getday('monday','1');
    if(isset($_GET['count'])){
      $cnt = $_GET['count'];
      getday('monday',$cnt);
    }
  }elseif($day == 'tuesday'){
    $qgetsum = "SELECT * FROM periode WHERE status = '1' AND hari_keluar='$day' LIMIT 200";
       getday('tuesday','1');
    if(isset($_GET['count'])){
      $cnt = $_GET['count'];
      getday('tuesday',$cnt);
    }
  }elseif($day == 'wednesday'){
    $qgetsum = "SELECT * FROM periode WHERE status = '1' AND hari_keluar='$day' LIMIT 200";
       getday('wednesday','1');
    if(isset($_GET['count'])){
      $cnt = $_GET['count'];
      getday('wednesday',$cnt);
    }
  }elseif($day == 'thursday'){
    $qgetsum = "SELECT * FROM periode WHERE status = '1' AND hari_keluar='$day' LIMIT 200";
    
    if(isset($_GET['count'])){
      $cnt = $_GET['count'];
      getday('Thursday',$cnt);
      echo 'haha';
    }else{
      getday('Thursday','1');
    }
  }elseif($day == 'friday'){
    $qgetsum = "SELECT * FROM periode WHERE status = '1' AND hari_keluar='$day' LIMIT 200";
    getday('friday','1');
    if(isset($_GET['count'])){
      $cnt = $_GET['count'];
      getday('friday',$cnt);
    }
  }elseif($day == 'saturday'){
    $qgetsum = "SELECT * FROM periode WHERE status = '1' AND hari_keluar='$day' LIMIT 200";
    getday('saturday','1');
    if(isset($_GET['count'])){
      $cnt = $_GET['count'];
      getday('saturday',$cnt);
    }
  }
}else{
  $qgetsum = "SELECT * FROM periode WHERE status='1' LIMIT 200";
if(isset($_GET['count'])){
  $cnt = $_GET['count'];
  limitperiode($cnt);
}else{
  limitperiode('1');
}
}
?>
                    </tbody>
                  </table>
                </div><nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
<?php

$getsum = mysqli_query($con,$qgetsum);
$sum = mysqli_num_rows($getsum);
$bttcount = ceil($sum / 10);
//echo $bttcount;
for ($k = 1 ; $k <= $bttcount; $k++){
  if(isset($_GET['day'])){
 echo '<li><a href="index.php?day='.$day.'&count='.$k.'">'.$k.'</a></li>'; 
  }else{
     echo '<li><a href="index.php?count='.$k.'">'.$k.'</a></li>'; 
  }

};
?>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav></div>
<div class="col-xs-3">
              <div class="common_head">Daily results</div>
              <div class="side_chart">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="chart_2">
                           <tbody><tr>
                    <td align="left" valign="middle"><a href="index.php?day=sunday">sunday</a></td>
                    <td align="center" valign="middle"><img src="img//pic_1.gif" width="50" height="31" alt=""></td>
                  </tr>
           
                             <tr>
                    <td align="left" valign="middle"><a href="index.php?day=monday">monday</a></td>
                    <td align="center" valign="middle"><img src="img//pic_2.gif" width="50" height="31" alt=""></td>
                  </tr>
           
                             <tr>
                    <td align="left" valign="middle"><a href="index.php?day=tuesday">tuesday</a></td>
                    <td align="center" valign="middle"><img src="img//pic_3.gif" width="50" height="31" alt=""></td>
                  </tr>
           
                             <tr>
                    <td align="left" valign="middle"><a href="index.php?day=wednesday">wednesday</a></td>
                    <td align="center" valign="middle"><img src="img//pic_4.gif" width="50" height="31" alt=""></td>
                  </tr>
           
                             <tr>
                    <td align="left" valign="middle"><a href="index.php?day=thursday">thursday</a></td>
                    <td align="center" valign="middle"><img src="img//pic_5.gif" width="50" height="31" alt=""></td>
                  </tr>
           
                             <tr>
                    <td align="left" valign="middle"><a href="index.php?day=friday">friday</a></td>
                    <td align="center" valign="middle"><img src="img//pic_6.gif" width="50" height="31" alt=""></td>
                  </tr>
           
                             <tr>
                    <td align="left" valign="middle"><a href="index.php?day=saturday">saturday</a></td>
                    <td align="center" valign="middle"><img src="img//pic_7.gif" width="50" height="31" alt=""></td>
                  </tr>
                </tbody></table></div></div></div>
                </div>
              </div>
            </div>

<div class="footer">
  <div id="layout-footer">
    <footer>
      <div class="content-border"></div>
      <div id="bottom-shadow"></div>

      <div class="copyRight">
          <br/><br/>
          <span class="block1">Copyright ©2014 - Hawaii Lottery</span>
          <span class="block2">All right reserved <a href="index.html">hawaii-lottery.net</a></span>
      </div>
    </footer>
  </div>
</div>
</body>
</html>

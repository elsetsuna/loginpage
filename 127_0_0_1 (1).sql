-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2017 at 06:48 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--
CREATE DATABASE IF NOT EXISTS `login` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `login`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user_id` int(11) NOT NULL COMMENT 'auto incrementing user_id of each user, unique index',
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `user_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logintime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user_id`, `user_name`, `user_password_hash`, `user_email`, `user_status`, `logintime`) VALUES
(3, 'asdasd', '$2y$10$iFDtFTow.2JRtEG9/31Xe.asJ1ZZlACeU65Ranc2dL7BwgI9LuFxS', 'a@a.com', 'super_user', '2016-06-11 09:38:46');

-- --------------------------------------------------------

--
-- Table structure for table `prize1`
--

CREATE TABLE `prize1` (
  `no` int(11) NOT NULL,
  `angka_pemenang` int(11) NOT NULL,
  `tanggal_keluar` text NOT NULL,
  `hari_keluar` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prize1`
--

INSERT INTO `prize1` (`no`, `angka_pemenang`, `tanggal_keluar`, `hari_keluar`, `status`) VALUES
(2, 1234, '09-12-2017', 'Saturday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prize2`
--

CREATE TABLE `prize2` (
  `no` int(11) NOT NULL,
  `angka_pemenang` int(11) NOT NULL,
  `tanggal_keluar` text NOT NULL,
  `hari_keluar` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prize2`
--

INSERT INTO `prize2` (`no`, `angka_pemenang`, `tanggal_keluar`, `hari_keluar`, `status`) VALUES
(2, 1234, '09-12-2017', 'Saturday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prize3`
--

CREATE TABLE `prize3` (
  `no` int(11) NOT NULL,
  `angka_pemenang` int(11) NOT NULL,
  `tanggal_keluar` text NOT NULL,
  `hari_keluar` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prize3`
--

INSERT INTO `prize3` (`no`, `angka_pemenang`, `tanggal_keluar`, `hari_keluar`, `status`) VALUES
(2, 1234, '09-12-2017', 'Saturday', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `prize1`
--
ALTER TABLE `prize1`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `prize2`
--
ALTER TABLE `prize2`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `prize3`
--
ALTER TABLE `prize3`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prize1`
--
ALTER TABLE `prize1`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prize2`
--
ALTER TABLE `prize2`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prize3`
--
ALTER TABLE `prize3`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
